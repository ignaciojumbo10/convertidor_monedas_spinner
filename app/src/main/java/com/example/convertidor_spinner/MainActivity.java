package com.example.convertidor_spinner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Spinner spinner;
    private TextView resultado;
    private EditText num;
    private Button boton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //obtenemos los datos de la interfaz
        spinner = findViewById(R.id.spinner);
        resultado = findViewById(R.id.txtresultado);
        num = findViewById(R.id.txtnumero);
        boton = findViewById(R.id.bnconvertir);

        //damos la accion al boton
        boton.setOnClickListener(this);

        //se crea un arreglo para el spinner
        String opciones[] = {"Escoja la opcion","dolar a euro","euro a dolar","dolar a pesos mexicanos","dolar a pesos argentinos","dolar a soles"};

        //sirve para cominicarnos y mostar los datos en el spinner // le envamos tres cosas el contexto, el tipo de spinner y el arreglo de las opciones
        ArrayAdapter <String> adapter = new ArrayAdapter<>(this, R.layout.spinner_item_opciones, opciones);

        //le agragamos al spinner
        spinner.setAdapter(adapter);

    }

    // metodo para convertir
    public void Convertir(){

        // formato para que salga con pocos decimales
        DecimalFormat format1 = new DecimalFormat("0.000");

        int op = spinner.getSelectedItemPosition();

        double numero = Double.parseDouble(String.valueOf(num.getText()));
        double res = 0;

        if(op ==0){
            // verificamos que el usuario escoja una opcion
            Toast.makeText(this,"Escoja un opcion",Toast.LENGTH_LONG).show();

        }else{
            if(op == 1){

                res = numero * 0.848033;
                resultado.setText(String.valueOf(format1.format(res)) + " Euros");

            }else if(op == 2){

                res = numero * 1.18;
                resultado.setText(String.valueOf(format1.format(res)) + " Dolares");

            } else if(op == 3){

                res = numero * 20.192704240779;
                 resultado.setText(String.valueOf(format1.format(res)) + " Pesos mexicanos");

            } else if(op == 4){

                res = numero * 96.367386363636;
                resultado.setText(String.valueOf(format1.format(res)) + " Pesos argentinos");

            } else if(op == 5){

                res = numero * 3.94;
                resultado.setText(String.valueOf(format1.format(res)) + " Soles");

          }
    }

    }
    @Override
    public void onClick(View v) {
        if(v == boton){
            if(num.getText().toString().isEmpty()){
                num.setError("Ingrese la cantidad a convertir");
                num.requestFocus();
            }else{
                Convertir();
            }

        }

    }
}